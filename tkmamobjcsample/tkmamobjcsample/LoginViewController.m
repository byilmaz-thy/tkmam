//
//  ViewController.m
//  tkmamobjcsample
//
//  Created by YAVUZ ÖZTÜRK on 30/01/2017.
//  Copyright © 2017 Turkish Airlines INC. All rights reserved.
//

#import "LoginViewController.h"

NSString *const TKMAMDOMAIN = @"MobileServiceDomain";
NSString *const TKMAMACCESGROUP = @"35MYG4AN5S.com.turkishairlines";//SSO özelliği için keychain özelliği aktif edilmeli. Keychain group olarak com.turkishairlines eklenmelidir. THY tarafında paketleme yapılırken de set edilebilir.
//NSString *const TKMAMURL = @"https://oammobiletest.thy.com";
NSString *const TKMAMURL = @"https://oammobiletest.thy.com";// PRODUCTION
NSString *const TKMAMAPPNAME = @"CargoPortalMobile";
NSString *const TKMAMSSOAPPNAME = @"OICSSOApp1";


@interface LoginViewController ()
    
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [TKMAMRestClient initializeWithDomain:TKMAMDOMAIN
                                      url:TKMAMURL
                                  appName:TKMAMAPPNAME
                               ssoAppName:TKMAMSSOAPPNAME
                               accesGroup:TKMAMACCESGROUP
                      loginViewController:self
                                 delegate:self];
    
    [TKMAMRestClient sharedInstance].authenticationRequiredCompletion = ^{
        // go back to loginViewController
#warning burası önemli, session time out oldugunda buraya dusuyor, login ekranına geri gelinmeli.
    };
    
    [[TKMAMRestClient sharedInstance] setup];
    _loginButton.enabled = NO;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)TKMAM_didStartSetup{
    //show progress
}

- (void)TKMAM_didFinishSetup{
    //dismiss progress
    //enable login button
    _loginButton.enabled = YES;
}

- (void)TKMAM_didFailSetup:(NSString *)message errorCode:(NSString *)errorCode{
    //dismiss progress
    //show errors
}

- (void)TKMAM_didFinishAuthentication:(NSString *)username{
    //dismiss progress
    [self performSegueWithIdentifier:@"LoginSuccesSegue" sender:nil];
    //useful methods
    //[[TKMAMRestClient sharedInstance] getUserName]; //null if not logged in
    //[[TKMAMRestClient sharedInstance] logout];
}

- (void)TKMAM_didFailAuthentication:(NSString *)title message:(NSString *)message errorCode:(NSString *)errorCode{
    //dismiss progress
    //show errors
}

- (void)TKMAM_willRequestTouchID {
     //dismiss progress
}

- (void)TKMAM_willStartAuthenticationWithTouchID {
    //show progress
}

- (IBAction)loginAction:(UIButton *)sender {
    [[TKMAMRestClient sharedInstance] authenticate:_usernameTextField.text password:_passwordTextField.text];
    //show progress
}
@end
