//
//  ViewController.h
//  tkmamobjcsample
//
//  Created by YAVUZ ÖZTÜRK on 30/01/2017.
//  Copyright © 2017 Turkish Airlines INC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <tkmam_standalone/tkmam_standalone-Swift.h>
/* aşağıdaki ayarların yapılması gerekiyor. (ilgili target build settings den) TKMAM frameworkün çalışabilmesi için
 
 //:configuration = Debug
 ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES = YES
 
 //:configuration = Release
 ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES = YES
 
 //:completeSettings = some
 ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES

 
 */

@interface LoginViewController : UIViewController <TKMAMRestClientDelegate>{
    
}

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

- (IBAction)loginAction:(UIButton *)sender;

@end

