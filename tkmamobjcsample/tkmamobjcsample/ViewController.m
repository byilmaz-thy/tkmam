//
//  ViewController.m
//  tkmamobjcsample
//
//  Created by YAVUZ ÖZTÜRK on 30/01/2017.
//  Copyright © 2017 Turkish Airlines INC. All rights reserved.
//

#import "ViewController.h"


NSString *const PATH = @"sampleRequest";
NSString *const BASEURL = @"https://kurumsalmob.thy.com/saglikmobil/";
NSString *const PRODBASEURL = @"https://wsmobile.thy.com/saglikmobil/";

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [self sampleRequest];
}

- (void)sampleRequest{
    //
    NSMutableURLRequest *sampleRequest = [[NSMutableURLRequest alloc] init];
    [sampleRequest setURL:[NSURL URLWithString:[BASEURL stringByAppendingString:PATH]]];
    [sampleRequest setHTTPMethod:@"GET"];
    [sampleRequest setAllHTTPHeaderFields:[[TKMAMRestClient sharedInstance] accessHeaders]]; // <----------------
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:sampleRequest
                                                                     completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                                         
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        if ([httpResponse statusCode] == 401) {
                                                                                //request acces tokens
                                                                            //Acces token SADECE base url için istenmeli
                                                                            [[TKMAMRestClient sharedInstance] getAccesToken:BASEURL completion:^(BOOL succes) {
                                                                                if (succes) {
                                                                                    //acces token acquired //repeat request
                                                                                    [self sampleRequest];
                                                                                } // access token alınamaması durumunda authenticationRequiredCompletion cagırılmaktadır.
                                                                            }];
                                                                        }else{
                                                                            NSLog(@"request success - şuanki url 404 atıyor. böyle bir servis olmadıgı için, 404 gelmesi acces token geçerli demek.");
                                                                        }
                                                                        
                                                                     }];
    [dataTask resume];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
