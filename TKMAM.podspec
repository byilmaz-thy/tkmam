#
# Be sure to run `pod lib lint TKMAM.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TKMAM'
  s.version          = '2.0.0'
  s.summary          = 'A framework for Turkish Airlines iOS mobile applications.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This is a mobile access manager framework for Turkish Airlines enterprise ios applications. Provides authentication and authorization.
                       DESC

  s.homepage         = 'https://bitbucket.thy.com/projects/TKMAM/repos/tkmam-ios-framework/browse'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'y_ozturk9' => 'y.ozturk@thy.com' }
  s.source           = { :git => 'https://byilmaz-thy@bitbucket.org/byilmaz-thy/tkmam.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.requires_arc = true
  s.source_files = 'TKMAM/Classes/**/*'
  s.swift_versions = ['4.2','5.0']
  # s.resource_bundles = {
  #   'TKMAM' => ['TKMAM/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'

  s.frameworks = 'LocalAuthentication', 'Foundation', 'UIKit', 'SystemConfiguration', 'CoreTelephony'

  s.dependency 'Alamofire', '~> 4.7'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.2' }

end
