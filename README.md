# TKMAM

[![CI Status](http://img.shields.io/travis/yvzzztrk/TKMAM.svg?style=flat)](https://travis-ci.org/yvzzztrk/TKMAM)
[![Version](https://img.shields.io/cocoapods/v/TKMAM.svg?style=flat)](http://cocoapods.org/pods/TKMAM)
[![License](https://img.shields.io/cocoapods/l/TKMAM.svg?style=flat)](http://cocoapods.org/pods/TKMAM)
[![Platform](https://img.shields.io/cocoapods/p/TKMAM.svg?style=flat)](http://cocoapods.org/pods/TKMAM)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## How to update TKMAM

    * commit,  all changes
    * git tag 'x.x.x' // version
    * git push --tags
    * pod spec lint
    * pod repo push cmc-specs TKMAM.podspec

## Requirements

## TKMAM Framework CocoaPods Integration

**If exists, remove embedded TKMAM framework from all targets where:**
- General Settings
- Embedded Libraries
- Linked Frameworks and Libraries

**run this script to add TKMAM to your local pod repository**

```sh
pod repo add cmc-specs https://bitbucket.thy.com/scm/ts/pod-specs.git
```
**Specify TKMAM into your project's Podfile:**
```ruby
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.thy.com/scm/ts/pod-specs.git'

pod 'TKMAM', '~> <TKMAM Version>'
```

## Author

yvzzztrk, y.ozturk@thy.com

## License

TKMAM is available under the MIT license. See the LICENSE file for more info.
