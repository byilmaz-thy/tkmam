//
//  ViewController.swift
//  tkmamswiftsample
//
//  Created by YAVUZ ÖZTÜRK on 21/11/2017.
//  Copyright © 2017 Turkish Airlines. All rights reserved.
//

import UIKit
import tkmam_standalone
import Alamofire

class ViewController: UIViewController {

    let endpoint: String = "https://sample.thy.com/sample/" // bağlanılacak servis acces manager arkasına alınmalıdır. Bu tanımları network güvenlik ve spweb yapmaktadır.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        sampleRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sampleRequest(){
        let headers = TKMAMRestClient.sharedInstance().accessHeaders() // acces manager arkasına alınmış (protected resource) a erişebilmek için gerekli headerlar
        
        Alamofire.request(endpoint, method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                
                if let HTTPURLResponse = response.response {
                    if HTTPURLResponse.statusCode == 401 { // 401 alındığı taktirde, TKMAM frameworkunden yeni access token istenmelidir.
                        TKMAMRestClient.sharedInstance().getAccesToken(self.endpoint, completion: { (succes) in // istenilen access token endpoint için geçerlidir. Örn: https://sample.thy.com/sample/ için alınırsa https://sample.thy.com/sample/* (wildcard) için geçerli olmaktadır.
                            if succes { // access token alındığı takdirde request tekrar edilmelidir.
                                self.sampleRequest()
                            } else {
                                debugPrint(response) //
                            }
                        })
                    } else {
                        debugPrint(response) // başarılı request
                    }
                } else {
                    debugPrint(response)
                }
                
        }
    }


}

