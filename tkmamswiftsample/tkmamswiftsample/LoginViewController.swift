//
//  LoginViewController.swift
//  tkmamswiftsample
//
//  Created by YAVUZ ÖZTÜRK on 21/11/2017.
//  Copyright © 2017 Turkish Airlines. All rights reserved.
//

import UIKit
import tkmam_standalone
import PKHUD


let TKMAMDOMAIN: String = "MobileServiceDomain"
let TKMAMACCESGROUP: String = "35MYG4AN5S.com.turkishairlines" //SSO özelliği için keychain özelliği aktif edilmeli. Keychain group olarak com.turkishairlines eklenmelidir.
let TKMAMURL: String = "https://oammobiletest.thy.com"
let TKMAMAPPNAME: String = "TKGenericProject" // AccessManager console dan oluşturulan Client App
let TKMAMSSOAPPNAME: String = "OICSSOApp1" // AccessManager console dan oluşturulan Agent App

// bu ayarlar örnek proje içindir, lab ortamında bulunan access manager rest apisini kullanmaktadır. sadece TKNet e bağlı iken ulaşılabilir. Prod ortamında yeni bir proje oluşturmak için network ve güvenlik müdürlüğü ile iletişime gecilmelidir.


class LoginViewController : UIViewController, TKMAMRestClientDelegate {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TKMAMRestClient.initialize(domain: TKMAMDOMAIN,
                                   url: TKMAMURL,
                                   appName: TKMAMAPPNAME,
                                   ssoAppName: TKMAMSSOAPPNAME,
                                   accesGroup: TKMAMACCESGROUP,
                                   loginViewController: self,
                                   delegate: self)

        
        TKMAMRestClient.sharedInstance().setup() // init ardından hemen setup çalıştırılmalı
        
        TKMAMRestClient.sharedInstance().authenticationRequiredCompletion = {
            //api request anında erişim sağlanmıyorsa bu kod bloğu çalışmaktadır. Yani kullanıcıdan tekrar username ve password istenmelidir. Login ekranına geri dönülmelidir.
        }
        
        if let username = TKMAMRestClient.sharedInstance().getUserName() { // eğer önceden login olduysa ve username var ise
            usernameTextField.text = username
        }
        
    }
    
    func TKMAM_didStartSetup() {
        Swift.debugPrint("didStartSetup")
        self.loginButton.isEnabled = false
        DispatchQueue.main.async {
            HUD.show(.labeledProgress(title: "Please wait", subtitle: "Initializing.."))
        }
        
    }
    
    func TKMAM_didFinishSetup() {
        Swift.debugPrint("didFinishSetup")
        self.loginButton.isEnabled = true
        DispatchQueue.main.async {
            HUD.hide()
        }
        
    }
    
    func TKMAM_didFailSetup(_ message: String, errorCode: String) {
        Swift.debugPrint("didFailSetup")
        DispatchQueue.main.async {
            HUD.hide()
            AppDelegate.showAlert(title: "ACM-\(errorCode)", message: message)
        }
    }
    
    func TKMAM_didFinishAuthentication(_ username: String) {
        Swift.debugPrint("didFinishAuthentication \(username)")
        DispatchQueue.main.async {
            HUD.hide()
            self.performSegue(withIdentifier: "InitialViewSegue", sender: nil)
        }
    }
    
    func TKMAM_didFailAuthentication(_ title: String, message: String, errorCode: String) {
        Swift.debugPrint("didFailAuthentication")
        self.loginButton.isEnabled = true
        DispatchQueue.main.async {
            HUD.hide()
            AppDelegate.showAlert(title: "ACM-\(errorCode)", message: message)
        }
        
    }
    
    func TKMAM_willRequestTouchID() {
        DispatchQueue.main.async {
            HUD.hide()
        }
    }
    
    func TKMAM_willStartAuthenticationWithTouchID() {
        DispatchQueue.main.async {
            HUD.show(.labeledProgress(title: "wait", subtitle: "initializing"))
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "InitialViewSegue" {
        }
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        HUD.show(.labeledProgress(title: "Please wait", subtitle: "Authenticating.."))
        TKMAMRestClient.sharedInstance().authenticate(usernameTextField.text!, password: passwordTextField.text!)
        self.loginButton.isEnabled = false
    }
}
