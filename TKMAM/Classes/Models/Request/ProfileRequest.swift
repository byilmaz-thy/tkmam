//
//  ProfileRequest.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 10.08.2018.
//

import Foundation

class ProfileRequest: JsonConvertable {
    
    let appId:              String
    let deviceUUID:         String
    let brand:              String
    let model:              String
    let platform:           String
    let osVersion:          String
    let appBinaryVersion:   String
    
    init(appId:             String,
         deviceUUID:        String,
         brand:             String,
         model:             String,
         platform:          String,
         osVersion:         String,
         appBinaryVersion:  String) {
        
        self.appId              = appId
        self.deviceUUID         = deviceUUID
        self.brand              = brand
        self.model              = model
        self.platform           = platform
        self.osVersion          = osVersion
        self.appBinaryVersion   = appBinaryVersion
        
    }
    
    func toJson() -> JSON {
        var json = JSON()
        
        json["appId"]               = self.appId            as AnyObject?
        json["deviceUUID"]          = self.deviceUUID       as AnyObject?
        json["brand"]               = self.brand            as AnyObject?
        json["model"]               = self.model            as AnyObject?
        json["platform"]            = self.platform         as AnyObject?
        json["osVersion"]           = self.osVersion        as AnyObject?
        json["appBinaryVersion"]    = self.appBinaryVersion as AnyObject?
        
        return json
    }
    
}
