//
//  RegisterRequest.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines. All rights reserved.
//

import Foundation

class RegisterRequest: JsonConvertable {
    
    let subjectType:        String
    let username:           String
    let password:           String
    let tokenTypeToCreate:  String
    let deviceProfile:      DeviceProfile
    let clientId:           String?
    
    init(subjectType:       String,
         username:          String,
         password:          String,
         tokenTypeToCreate: String,
         deviceProfile:     DeviceProfile,
         clientId:          String? = nil
        )
    {
        
        self.subjectType        = subjectType
        self.username           = username
        self.password           = password
        self.tokenTypeToCreate  = tokenTypeToCreate
        self.deviceProfile      = deviceProfile
        self.clientId           = clientId
        
    }
    
    func toJson() -> JSON {
        var json = JSON()
        
        json["X-Idaas-Rest-Subject-Type"]               = self.subjectType              as AnyObject?
        json["X-Idaas-Rest-Subject-Username"]           = self.username                 as AnyObject?
        json["X-Idaas-Rest-Subject-Password"]           = self.password                 as AnyObject?
        json["X-Idaas-Rest-New-Token-Type-To-Create"]   = self.tokenTypeToCreate        as AnyObject?
        json["deviceProfile"]                           = self.deviceProfile.toJson()   as AnyObject?
        
        if let clientId = self.clientId { json["clientId"] = clientId as AnyObject? }
        
        return json
    }
    
}
