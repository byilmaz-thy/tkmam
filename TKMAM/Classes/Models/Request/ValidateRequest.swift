//
//  ValidateRequest.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines INC. All rights reserved.
//

import Foundation

class ValidateRequest : JsonConvertable {
    
    let subjectValue: String
    let subjectType : String
    
    init(subjectValue: String,
         subjectType:   String)
    {
        self.subjectType    = subjectType
        self.subjectValue   = subjectValue
    }
    
    func toJson() -> JSON {
        
        var json = JSON()
        
        json["X-Idaas-Rest-Subject-Value"] = self.subjectValue  as AnyObject?
        json["X-Idaas-Rest-Subject-Type"]  = self.subjectType   as AnyObject?
        
        return json
    }
    
}

