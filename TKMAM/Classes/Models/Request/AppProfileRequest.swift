//
//  AppProfileRequest.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 18.07.2018.
//  Copyright © 2018 Turkish Airlines. All rights reserved.
//

import Foundation

class AppProfileRequest: JsonConvertable {
    
    let serviceDomain:      String
    let osType:             String
    let osVer:              String
    let clientSDKVersion:   String
    
    init(serviceDomain:     String,
        osType:             String,
        osVer:              String,
        clientSDKVersion:   String)
    {
        self.serviceDomain      = serviceDomain
        self.osType             = osType
        self.osVer              = osVer
        self.clientSDKVersion   = clientSDKVersion
    }
    
    func toJson() -> JSON {
        var json = JSON()
        
        json["serviceDomain"]       = self.serviceDomain    as AnyObject?
        json["osType"]              = self.osType           as AnyObject?
        json["osVer"]               = self.osVer            as AnyObject?
        json["clientSDKVersion"]    = self.clientSDKVersion as AnyObject?
        
        return json
    }
}
