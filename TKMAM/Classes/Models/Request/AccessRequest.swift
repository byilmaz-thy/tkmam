//
//  AccessRequest.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines INC. All rights reserved.
//

import Foundation

class AccessRequest: JsonConvertable {
    
    let subjectValue:           String
    let applicationContext:     String
    let applicationResource:    String
    let subjectType:            String
    let tokenTypeToCreate:      String
    
    init(subjectValue:          String,
         applicationContext:    String,
         applicationResource:   String,
         subjectType:           String,
         tokenTypeToCreate:     String)
    {
        
        self.subjectValue           = subjectValue
        self.applicationContext     = applicationContext
        self.applicationResource    = applicationResource
        self.subjectType            = subjectType
        self.tokenTypeToCreate      = tokenTypeToCreate
    }
    
    func toJson() -> JSON {
        var json = JSON()
        
        json["X-Idaas-Rest-Subject-Value"]              = self.subjectValue         as AnyObject
        json["X-Idaas-Rest-Application-Context"]        = self.applicationContext   as AnyObject
        json["X-Idaas-Rest-Application-Resource"]       = self.applicationResource  as AnyObject
        json["X-Idaas-Rest-Subject-Type"]               = self.subjectType          as AnyObject
        json["X-Idaas-Rest-New-Token-Type-To-Create"]   = self.tokenTypeToCreate    as AnyObject
        
        return json
    }
}
