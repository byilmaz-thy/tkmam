//
//  ACMError.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines INC. All rights reserved.
//

import Foundation

class ACMError : JsonInitializable {
    
    static let jailBreak                  = ACMError(oicErrorCode: "11000",     message: "Jail Broken devices are not allowed to use this app.")
    static let noInternetConnection_1     = ACMError(oicErrorCode: "11009-1",   message: "Please check your internet connection.")
    static let noInternetConnection_2     = ACMError(oicErrorCode: "11009-2",   message: "Please check your internet connection.")
    static let noInternetConnection_3     = ACMError(oicErrorCode: "11009-3",   message: "Please check your internet connection.")
    static let noInternetConnection_4     = ACMError(oicErrorCode: "11009-4",   message: "Please check your internet connection.")
    static let usernameOrPassword         = ACMError(oicErrorCode: "11001",     message: "Username or password can not be empty.")
    static let wrongUsernameOrPassword    = ACMError(oicErrorCode: "11012",     message: "Wrong username or password.")
    static let somethingWentWrong         = ACMError(oicErrorCode: "11003",     message: "Something went wrong, please try again later.")
    static let somethingWentWrong_2       = ACMError(oicErrorCode: "11004",     message: "Something went wrong, please try again later.")
    static let notAuthorized              = ACMError(oicErrorCode: "11005",     message: "You are not allowed to use this application.")

    
    public let oicErrorCode:  String?
    public let message:       String
    
    required init(json: JSON) throws {
        
        self.oicErrorCode   = try json.getOptional("oicErrorCode")
        self.message        = try json.get("message")
        
    }
    
    init(oicErrorCode:  String?,
         message:       String) {
        
        self.oicErrorCode   = oicErrorCode
        self.message        = message
    }
}

extension ACMError {
    
    func showAlert() {
        Alert.show(title:   "Error",
                   message: message + " (" + (oicErrorCode ?? "") + ")")
    }
}
