//
//  DeviceProfile.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines. All rights reserved.
//

import Foundation

class DeviceProfile: JsonConvertable {
    
    static let TKMAM_OS_TYPE            = "iPhone OS"
    static let TKMAM_CLIENT_SDK_VERSION = "11.1.2.0.0"
    
    let sdkVersion:         String
    let udid:               String
    let osType:             String
    let locale:             String
    let osversion:          String
    let phonecarriername:   String
    let jailBroken:         Bool
    
    init(sdkVersion:        String,
         udid:              String,
         osType:            String,
         locale:            String,
         osversion:         String,
         phonecarriername:  String,
         jailBroken:        Bool)
    {
        self.sdkVersion         = sdkVersion
        self.udid               = udid
        self.osType             = osType
        self.locale             = locale
        self.osversion          = osversion
        self.phonecarriername   = phonecarriername
        self.jailBroken         = jailBroken
    }
    
    func toJson() -> JSON {
        var json = JSON()
        
        json["oracle:idm:claims:client:sdkversion"]         = self.sdkVersion                                               as AnyObject?
        json["hardwareIds"]                                 = ["oracle:idm:claims:client:udid" : self.udid as AnyObject?]   as AnyObject?
        json["oracle:idm:claims:client:jailbroken"]         = self.jailBroken                                               as AnyObject?
        json["oracle:idm:claims:client:ostype"]             = self.osType                                                   as AnyObject?
        json["oracle:idm:claims:client:locale"]             = self.locale                                                   as AnyObject?
        json["oracle:idm:claims:client:osversion"]          = self.osversion                                                as AnyObject?
        json["oracle:idm:claims:client:phonecarriername"]   = self.phonecarriername                                         as AnyObject?
        
        return json
    }
    
}
