//
//  MobileAppConfig.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 15/12/16.
//  Copyright © 2016 Turkish Airlines INC. All rights reserved.
//

import Foundation

class MobileAppConfig : JsonInitializable, JsonConvertable{
    
    let AllowOfflineAuthentication : String
    let auto_login : String
    let AuthenticationRetryCount : String
    let app_version : String
    let download_url : String
    let force_update : String
    let jailbreak_detection : String
    let pin_enabled: String
    
    required init(json: JSON) throws {
        
        self.AllowOfflineAuthentication = try json.get("AllowOfflineAuthentication")
        self.auto_login = try json.get("auto_login")
        self.AuthenticationRetryCount = try json.get("AuthenticationRetryCount")
        self.app_version = try json.get("ios_app_version")
        self.download_url = try json.get("download_url")
        self.force_update = try json.get("force_update")
        self.jailbreak_detection = try json.get("jailbreak_detection")
        self.pin_enabled = try json.get("pin_enabled")
        
    }
    
    func toJson() -> JSON {
        var json : JSON = JSON()
        
        json["AllowOfflineAuthentication"] = self.AllowOfflineAuthentication as AnyObject?
        json["auto_login"] = self.auto_login as AnyObject?
        json["AuthenticationRetryCount"] = self.AuthenticationRetryCount as AnyObject?
        json["ios_app_version"] = self.app_version as AnyObject?
        json["download_url"] = self.download_url as AnyObject?
        json["force_update"] = self.force_update as AnyObject?
        json["jailbreak_detection"] = self.jailbreak_detection as AnyObject?
        json["pin_enabled"] = self.pin_enabled as AnyObject?
        
        return json
    }
}
