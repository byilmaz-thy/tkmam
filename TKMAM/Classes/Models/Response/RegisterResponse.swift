//
//  RegisterResponse.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines INC. All rights reserved.
//

import Foundation

class RegisterResponse : JsonInitializable{
    
    let tokenType:  String
    let tokenValue: String
    
    required init(json: JSON) throws {
        
        self.tokenType  = try json.get("X-Idaas-Rest-Token-Type")
        self.tokenValue = try json.get("X-Idaas-Rest-Token-Value")
    }
    
}
