//
//  AuthenticateResponse.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines INC. All rights reserved.
//

import Foundation

class AuthenticateResponse: JsonInitializable {
    
    let providerType:   String
    let tokenType:      String
    let userPrincipal:  String
    let tokenValue:     String
    
    required init(json: JSON) throws {
        
        self.providerType   = try json.get("X-Idaas-Rest-Provider-Type")
        self.tokenType      = try json.get("X-Idaas-Rest-Token-Type")
        self.userPrincipal  = try json.get("X-Idaas-Rest-User-Principal")
        self.tokenValue     = try json.get("X-Idaas-Rest-Token-Value")
    }
    
}
