//
//  ProfileResponse.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 10.08.2018.
//

import Foundation

enum UpdateType: String {
    
    case none       = "0"
    case optional   = "1"
    case force      = "2"
    
}

class ProfileResponse: JsonInitializable, JsonConvertable {
    
    let userKey:                    String?
    let downloadUrl:                String
    let informUpdateFrequency:      Int
    let userAuthorizedToSeeProfile: Bool
    let jailBreakProtection:        Bool
    let updateType:                 UpdateType
    
    required init(json: JSON) throws {
        
        self.userKey                                = try json.getOptional("userKey")
        self.userAuthorizedToSeeProfile             = try json.get("userAuthorizedToSeeProfile")
        self.downloadUrl                            = try json.get("downloadUrl")
        self.jailBreakProtection                    = try json.getOptional("jailBreakProtection") ?? false
        self.updateType                             = try json.get("updateType")
        self.informUpdateFrequency                  = try json.get("informUpdateFrequency")
        
    }
    
    func toJson() -> JSON {
        var json = JSON()
        
        if let userKey = userKey { json["userKey"] = userKey as AnyObject? }
            
        json["userAuthorizedToSeeProfile"]          = self.userAuthorizedToSeeProfile   as AnyObject?
        json["downloadUrl"]                         = self.downloadUrl                  as AnyObject?
        
        json["jailBreakProtection"]                 = self.jailBreakProtection          as AnyObject?
        json["updateType"]                          = self.updateType.rawValue          as AnyObject?
        json["informUpdateFrequency"]               = self.informUpdateFrequency        as AnyObject?
        
        return json
    }
    
    func openDownloadUrl() {
        if UIApplication.shared.canOpenURL(URL(string: downloadUrl)!) {
            UIApplication.shared.openURL(URL(string: downloadUrl)!)
        }
    }
}
