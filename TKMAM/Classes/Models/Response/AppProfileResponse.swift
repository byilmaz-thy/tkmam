//
//  AppProfileResponse.swift
//  oamrest
//
//  Created by YAVUZ ÖZTÜRK on 18/10/16.
//  Copyright © 2016 Turkish Airlines INC. All rights reserved.
//

import Foundation

class AppProfileResponse : JsonInitializable, JsonConvertable {
    
    let serviceDomain:      String
    let accessService:      String
    let deleteService:      String
    let registerService:    String
    let userAuthnService:   String
    let userProfileService: String
    let validateService:    String
    let clientId:           String
//    let mobileAppConfig : MobileAppConfig
    
    required init(json: JSON) throws {
        
        self.serviceDomain      = try json.get("serviceDomain")
        self.accessService      = try json.get("accessService")
        self.deleteService      = try json.get("deleteService")
        self.registerService    = try json.get("registerService")
        self.userAuthnService   = try json.get("userAuthnService")
        self.userProfileService = try json.get("userProfileService")
        self.validateService    = try json.get("validateService")
        self.clientId           = try json.get("clientId")
//        self.mobileAppConfig = try json.get("mobileAppConfig")
    
    }
    
    func toJson() -> JSON {
        var json : JSON = JSON()
        
        json["serviceDomain"]       = self.serviceDomain        as AnyObject?
        json["accessService"]       = self.accessService        as AnyObject?
        json["deleteService"]       = self.deleteService        as AnyObject?
        json["registerService"]     = self.registerService      as AnyObject?
        json["userAuthnService"]    = self.userAuthnService     as AnyObject?
        json["userProfileService"]  = self.userProfileService   as AnyObject?
        json["validateService"]     = self.validateService      as AnyObject?
        json["clientId"]            = self.clientId             as AnyObject?
//        json["mobileAppConfig"] = self.mobileAppConfig.toJson() as AnyObject?
        
        return json
    }
    
//    func jailBreakDetection() -> Bool {
//        return mobileAppConfig.jailbreak_detection == "true"
//    }
//
//    func isForce() -> Bool {
//        return mobileAppConfig.force_update == "true"
//    }
//
//    func isAutoLogin() -> Bool {
//        return mobileAppConfig.auto_login == "true"
//    }
//
//    func allowsOfflineAuth() -> Bool {
//        return mobileAppConfig.AllowOfflineAuthentication == "true"
//    }
    
}
