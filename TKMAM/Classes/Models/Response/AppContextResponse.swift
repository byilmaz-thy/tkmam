//
//  AppContextResponse.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines. All rights reserved.
//

import Foundation

class AppContextResponse: JsonInitializable {
    
    let appContext: String
    
    required init(json: JSON) throws {
        
        appContext = try json.get("appContext")
    }
    
    init(appContext: String) {
        
        self.appContext = appContext
    }
}
