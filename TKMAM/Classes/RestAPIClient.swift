//
//  RestAPIClient.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 17.07.2018.
//  Copyright © 2018 Turkish Airlines INC. All rights reserved.
//


import Foundation
import Alamofire

typealias API = RestAPIClient

enum RestAPIClient: String {
    
    case AppProfile     = "/oic_rest/rest/AppProfiles/"
    case Register
    case Authenticate
    case Validate
    case Access
    case AppContext
    case Profile        = "/api/app/getAppProfile"
    
    typealias Request = (
        method:         HTTPMethod,
        body:           JsonConvertable?,
        responseType:   JsonInitializable.Type,
        encoding:       ParameterEncoding
    )
    
    private func url(with value: String = "",to store: Bool = false) -> String {
        switch self {
        case .AppProfile:
            return TKAccessManager.shared.accessManagerUrl + self.rawValue + value
        case .Register:
            return TKAccessManager.shared.accessManagerUrl + TokenVault.shared.appProfile!.registerService + value
        case .Authenticate:
            return TKAccessManager.shared.accessManagerUrl + TokenVault.shared.appProfile!.userAuthnService + value
        case .Validate:
            return TKAccessManager.shared.accessManagerUrl + TokenVault.shared.appProfile!.validateService + value
        case .Access:
            return TKAccessManager.shared.accessManagerUrl + TokenVault.shared.appProfile!.accessService + value
        case .AppContext:
            return store ? TKAccessManager.shared.storeUrl : TKAccessManager.shared.baseUrl
        case .Profile:
            return TKAccessManager.shared.storeUrl + self.rawValue
        }
        
    }
    
    private func header() -> [String: String]{
        return TokenVault.shared.requestHeaders(by: self)
    }
    
    private func router(value: String = "", body: JsonConvertable? = nil) -> Request? {
        
        switch self {
        case .AppProfile:
            return (.get, body, AppProfileResponse.self, URLEncoding.default)
        case .Register:
            return (.post, body, RegisterResponse.self, JSONEncoding.default)
        case .Authenticate:
            return (.post, body, AuthenticateResponse.self, JSONEncoding.default)
        case .Validate:
            return (.get, body, ValidateResponse.self, URLEncoding.default)
        case .Access:
            return (.post, body, AccessResponse.self, JSONEncoding.default)
        case .AppContext:
            return (.get, body, AppContextResponse.self, URLEncoding.default)
        case .Profile:
            return (.post, body, ProfileResponse.self, JSONEncoding.default)
        }
        
    }
    
    func sendRequest<T: JsonInitializable>(value:               String = "",
                                           body:                JsonConvertable? = nil,
                                           responseType:        T.Type,
                                           to store:            Bool = false,
                                           successsCompletion:  @escaping (_ response: T) -> (),
                                           errorCompletion:     @escaping (_ error: ACMError) -> (Bool))
        
    {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        guard let router = self.router(value: value, body: body) else {
            return
        }
        
        self.print(request: router, value: value)
        
        Alamofire.request(self.url(with: value, to: store),
                          method:       router.method,
                          parameters:   router.body != nil ? router.body!.toJson() : nil,
                          encoding:     router.encoding,
                          headers:      self.header())
            .validate(statusCode: Array(200..<300))
            .responseData
            { (dataResponse) in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                self.print(response: dataResponse)
                
                var acmError: ACMError?
                
                if self == .AppContext {
                    if let httpResponse = dataResponse.response{
                        if let WWW_Authenticate = httpResponse.allHeaderFields["Www-Authenticate"] as? String {
                            if let appContext = WWW_Authenticate.components(separatedBy: ", ").last?.replacingOccurrences(of: "request-ctx=\"", with: "").replacingOccurrences(of: "\"", with: "") {
                                successsCompletion(AppContextResponse(appContext: appContext) as! T)
                                return
                            }
                        }
                    }
                    
                    acmError = ACMError(oicErrorCode: "MissingRequestContext", message: "Request context failed.")
                    if let acmError = acmError, errorCompletion(acmError) {
                        acmError.showAlert()
                    }
                    return
                }
                
                dataResponse.result.ifSuccess {
                    
                    if let json = dataResponse.data?.toJson() {
                        
                        do {
                            let obj = try router.responseType.init(json: json) as? T
                            successsCompletion(obj!)
                        } catch let castingError as CastingError {
                            acmError = ACMError(oicErrorCode: "CastingError", message: castingError.errorMessage ?? "")
                        } catch _ { }
                    } else {
                        acmError = ACMError(oicErrorCode: "InvalidJSON", message: "")
                    }
                    
                    if let acmError = acmError, errorCompletion(acmError) {
                        acmError.showAlert()
                    }
                }
                
                
                dataResponse.result.ifFailure {
                    
                    if let json = dataResponse.data?.toJson() {
                        do {
                            acmError = try ACMError(json: json)
                        } catch let castingError as CastingError {
                            acmError = ACMError(oicErrorCode: "CastingError", message: castingError.errorMessage ?? "")
                        } catch _ { }
                    } else {
                        if let error = dataResponse.error as? AFError {
                            switch error {
                            case .invalidURL(let url):
                                acmError = ACMError(oicErrorCode: "InvalidURL", message: "\(url) - \(error.localizedDescription)")
                            case .parameterEncodingFailed(_):
                                acmError = ACMError(oicErrorCode: "ParameterEncodingFailed", message: error.localizedDescription)
                            case .multipartEncodingFailed(_):
                                acmError = ACMError(oicErrorCode: "MultipartEncodingFailed", message: error.localizedDescription)
                            case .responseValidationFailed(let reason):
                                switch reason {
                                case .dataFileNil, .dataFileReadFailed:
                                    acmError = ACMError(oicErrorCode: "ResponseValidationFailed", message: "Downloaded file could not be read")
                                case .missingContentType(let acceptableContentTypes):
                                    acmError = ACMError(oicErrorCode: "MissingContentType", message: "Content Type Missing: \(acceptableContentTypes)")
                                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                                    acmError = ACMError(oicErrorCode: "UnacceptableContentType", message: "Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                                case .unacceptableStatusCode(let code):
                                    acmError = ACMError(oicErrorCode: "UnacceptableStatusCode", message: "Response status code was unacceptable: \(code)")
                                }
                            case .responseSerializationFailed(_):
                                acmError = ACMError(oicErrorCode: "ResponseSerializationFailed", message: "Response serialization failed: \(error.localizedDescription)")
                            }
                        } else if let error = dataResponse.error as? URLError {
                            acmError = ACMError(oicErrorCode: "\(error.code.rawValue)", message: error.localizedDescription)
                        } else {
                            acmError = ACMError(oicErrorCode: "UNKNOWN ERROR", message: "\(dataResponse.error?.localizedDescription ?? "UNKNOWN")")
                        }
                    }
                    
                    if let acmError = acmError, errorCompletion(acmError) {
                        acmError.showAlert()
                    }
                    
                }
        }
        
    }
    
    fileprivate func print(response: DataResponse<Data>) {
        if TKMAM_DEBUG_MODE {
            Swift.print("**********************************************************************", terminator: "\n")
            Swift.print()
            Swift.print("TKMAM Request: \(String(describing: response.request))")
            Swift.print()
            Swift.print("TKMAM Response: \(String(describing: response.response))")
            Swift.print()
            if let json = response.data?.toJson() {
                Swift.print("TKMAM Response Body: \(json.toString(options: [.prettyPrinted]))")
            }
            Swift.print()
            Swift.print("TKMAM Error: \(String(describing: response.error))")
            Swift.print()
            Swift.print("**********************************************************************", terminator: "\n")
        }
    }
    
    fileprivate func print(request: Request, value: String = "") {
        if TKMAM_DEBUG_MODE {
            Swift.print("**********************************************************************", terminator: "\n")
            Swift.print()
            Swift.print(self.url(with: value), terminator: "\n")
            Swift.print()
            Swift.print(request.method.rawValue, terminator: "\n")
            Swift.print()
            Swift.print(request.body?.toJson().toString(options: [.prettyPrinted]) ?? "", terminator: "\n")
            Swift.print()
            Swift.print(request.encoding, terminator: "\n")
            Swift.print()
            Swift.print(self.header().toString(options: [.prettyPrinted]), terminator: "\n")
            Swift.print()
            Swift.print("**********************************************************************", terminator: "\n")
        }
    }
}













