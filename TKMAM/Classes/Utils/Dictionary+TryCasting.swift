//
//  Dictionary+TryCasting.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 18/10/16.
//  Copyright © 2016 Turkish Airlines INC. All rights reserved.
//

import Foundation

typealias JSON = [String : Any]

protocol JsonInitializable {
    init (json: JSON) throws
}

protocol JsonConvertable {
    func toJson() -> JSON
}

protocol DisplayableErrorType {
    
    var errorMessage: String? { get }
}

enum CastingError: Error, DisplayableErrorType {
    case failure(key: String)
    
    var errorMessage: String? {
        switch self {
        case .failure(let key):
            return "Casting Error: \(key)"
        }
    }
}

// Helper functions for Dictionary to get values with certain type or throw
extension Dictionary {
    
    func get<T>(_ key: String) throws -> T {
        guard let keyAs = key as? Key else {
            throw CastingError.failure(key: "-")
        }
        guard let obj = self[keyAs], let objAs = obj as? T else {
            throw CastingError.failure(key: key)
        }
        return objAs
    }
    
    func get<T: RawRepresentable>(_ key: String) throws -> T {
        guard let keyAs = key as? Key else {
            throw CastingError.failure(key: "-")
        }
        guard let obj = self[keyAs], let objAs = obj as? T.RawValue else {
            throw CastingError.failure(key: key)
        }
        guard let value = T(rawValue: objAs) else {
            throw CastingError.failure(key: key)
        }
        return value
    }
    
    func get<T: JsonInitializable>(_ key: String) throws -> T {
        guard let keyAs = key as? Key else {
            throw CastingError.failure(key: "-")
        }
        guard let obj = self[keyAs], let objAs = obj as? [String: AnyObject] else {
            throw CastingError.failure(key: key)
        }
        return try T(json: objAs)
    }
    
    func get<T: JsonInitializable>(_ key: String) throws -> [T] {
        guard let keyAs = key as? Key else {
            throw CastingError.failure(key: "-")
        }
        guard let obj = self[keyAs], let objAs = obj as? [[String: AnyObject]] else {
            throw CastingError.failure(key: key)
        }
        return try objAs.map { try T(json: $0) }
    }
    
    func convert<T: JsonInitializable>() throws -> [Key: T] {
        var result: [Key: T] = [:]
        for (key, innerObj) in self {
            guard let obj = innerObj as? [String: AnyObject] else {
                throw CastingError.failure(key: "Convert")
            }
            result[key] = try T(json: obj)
        }
        return result
    }
    
    func getOptional<T>(_ key: String) throws -> T? {
        guard let keyAs = key as? Key else {
            throw CastingError.failure(key: "-")
        }
        guard let obj = self[keyAs], !(obj is NSNull) else {
            return nil
        }
        guard let objAs = obj as? T else {
            throw CastingError.failure(key: key)
        }
        return objAs
    }
    
    func getOptional<T: RawRepresentable>(_ key: String) throws -> T? {
        guard let keyAs = key as? Key else {
            throw CastingError.failure(key: "-")
        }
        guard let obj = self[keyAs], !(obj is NSNull) else {
            return nil
        }
        guard let objAs = obj as? T.RawValue else {
            throw CastingError.failure(key: key)
        }
        guard let value = T(rawValue: objAs) else {
            throw CastingError.failure(key: key)
        }
        return value
    }
    
    func getOptional<T: JsonInitializable>(_ key: String) throws -> T? {
        guard let keyAs = key as? Key else {
            throw CastingError.failure(key: "-")
        }
        guard let obj = self[keyAs], !(obj is NSNull) else {
            return nil
        }
        guard let objAs = obj as? [String: AnyObject] else {
            throw CastingError.failure(key: key)
        }
        return try T(json: objAs)
    }
    
    func getOptional<T: JsonInitializable>(_ key: String) throws -> [T]? {
        guard let keyAs = key as? Key else {
            throw CastingError.failure(key: "-")
        }
        guard let obj = self[keyAs], !(obj is NSNull) else {
            return nil
        }
        guard let objAs = obj as? [[String: AnyObject]] else {
            throw CastingError.failure(key: key)
        }
        return try objAs.map { try T(json: $0) }
    }
}

extension String {
    func convertToJson () -> JSON {
        let data = self.data(using: String.Encoding.utf8)
        let json = try! JSONSerialization.jsonObject(with: data!, options:[JSONSerialization.ReadingOptions.allowFragments]) as! JSON
        return json
    }
    
    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        return data.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    }
}

extension Data {
    func toJson () -> JSON? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: []) as? JSON
        } catch {
            return nil
        }
    }
}

extension Dictionary where Key : ExpressibleByStringLiteral, Value : Any {
    func toString(options: JSONSerialization.WritingOptions = []) -> String {
        let data = try! JSONSerialization.data(withJSONObject: self as AnyObject, options: options)
        let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
        return string
    }
}

