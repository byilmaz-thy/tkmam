//
//  TKAlert.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 20.07.2018.
//  Copyright © 2018 Turkish Airlines. All rights reserved.
//

import Foundation

typealias Alert = TKAlert

class TKAlert {
    
    class func show(title: String, message: String, actions: [UIAlertAction]? = [UIAlertAction(title: "OK", style: .default, handler: nil)]) {
        DispatchQueue.main.async {
            let alertViewController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            actions?.forEach({ (action) in
                alertViewController.addAction(action)
            })
            
            // If any alert view is presented at this time
            if let presentedViewController = topViewController()?.presentedViewController {
                if presentedViewController.isKind(of: UIAlertController.classForCoder()) {
                    topViewController()?.dismiss(animated: true, completion: {
                        topViewController()!.present(alertViewController, animated: true, completion: nil)
                    })
                }
            } else {
                topViewController()!.present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}
