//
//  TKSecretKey.swift
//  TKMAMFramework
//
//  Created by YAVUZ ÖZTÜRK on 02/11/16.
//  Copyright © 2016 Turkish Airlines INC. All rights reserved.
//

import Foundation

typealias SecretKey = TKSecretKey

public class TKSecretKey : NSObject {
    
    private static let TKMAM_CLIENT_SECRET_KEY = "TKMAM_CLIENT_SECRET_KEY"
    
    open class func get(_ accessGroup: String) -> Data {
        let keychain = KeychainSwift()
        keychain.accessGroup = accessGroup
        if let secret = keychain.getData(SecretKey.TKMAM_CLIENT_SECRET_KEY) {
            return secret
        }
        var secret = Data(count: 64)
        _ = secret.withUnsafeMutableBytes { bytes in
            SecRandomCopyBytes(kSecRandomDefault, 64, bytes)
        }
        keychain.set(secret as Data, forKey: SecretKey.TKMAM_CLIENT_SECRET_KEY)
        return secret
    }
}
