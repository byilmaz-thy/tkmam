//
//  TKTokenVault.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 18.07.2018.
//  Copyright © 2018 Turkish Airlines. All rights reserved.
//

import Foundation

typealias TokenVault = TKTokenVault

class TKTokenVault {
    
    private var TKMAM_APPLICATION_PROFILE       = "TKMAM_APPLICATION_PROFILE3"
    private var TKMAM_STORE_PROFILE             = "TKMAM_STORE_PROFILE"
    private let TKMAM_CLIENT_REGISTRATION_TOKEN = "TKMAM_CLIENT_REGISTRATION_TOKEN"
    private let TKMAM_USER_TOKEN                = "TKMAM_USER_TOKEN"
//    private let TKMAM_ACCESS_TOKEN              = "TKMAM_ACCESS_TOKEN"
//    private let TKMAM_USERNAME                  = "TKMAM_USERNAME"
//    private let TKMAM_PASSWORD                  = "TKMAM_PASSWORD"
    
    static var shared: TokenVault!
    
    private let keychain = KeychainSwift()
    
    let domain:     String
    let ssoAppName: String
    
    var registrationToken: String? {
        didSet {
            if let registrationToken = registrationToken {
                set(key: TKMAM_CLIENT_REGISTRATION_TOKEN, value: registrationToken)
            }else{
                delete(key: TKMAM_CLIENT_REGISTRATION_TOKEN)
            }
        }
    }
    
    var userToken: String? {
        didSet {
            if let userToken = userToken {
                set(key: TKMAM_USER_TOKEN, value: userToken)
            }else{
                delete(key: TKMAM_USER_TOKEN)
            }
        }
    }
    
    //old profile from acm
    var appProfile: AppProfileResponse? {
        didSet {
            if let appProfile = appProfile {
                set(key: TKMAM_APPLICATION_PROFILE, value: appProfile.toJson().toString())
            }else{
                delete(key: TKMAM_APPLICATION_PROFILE)
            }
        }
    }
    
    // new profile from tkstore
    var profile: ProfileResponse? {
        didSet {
            if let profile = profile {
                set(key: TKMAM_STORE_PROFILE, value: profile.toJson().toString())
            }else{
                delete(key: TKMAM_STORE_PROFILE)
            }
        }
    }
    
    var accessToken : String?
    var storeAccessToken: String?
    
    open class func initialize(accessGroup: String,
                               binaryId:    String,
                               ssoAppName:  String,
                               domain:      String) {
        
        shared = TKTokenVault(accessGroup:  accessGroup,
                              binaryId:     binaryId,
                              ssoAppName:   ssoAppName,
                              domain:       domain)
    }
    
    private init(accessGroup:   String,
                 binaryId:      String,
                 ssoAppName:    String,
                 domain:        String) {
        
        TKMAM_APPLICATION_PROFILE   = TKMAM_APPLICATION_PROFILE + "-" + binaryId
        TKMAM_STORE_PROFILE         = TKMAM_STORE_PROFILE + "-" + binaryId
        
        self.domain     = domain
        self.ssoAppName = ssoAppName
        
        keychain.accessGroup = accessGroup
        
        registrationToken   = get(key: TKMAM_CLIENT_REGISTRATION_TOKEN)
        userToken           = get(key: TKMAM_USER_TOKEN)
        
        if let jsonString = get(key: TKMAM_APPLICATION_PROFILE) {
            appProfile  = try? AppProfileResponse(json: jsonString.convertToJson())
        }
        
        if let jsonString = get(key: TKMAM_STORE_PROFILE) {
            profile     = try? ProfileResponse(json: jsonString.convertToJson())
        }
        
    }
    
    func set(key: String, value: String){
        keychain.set(value, forKey: key)
    }
    
    func delete(key: String){
        keychain.delete(key)
    }
    
    func get(key: String) -> String? {
        return keychain.get(key)
    }
    
    // secure strings
    func setSecureString(key: String, value: String){
        keychain.setSecureString(value, key: key)
    }
       
    func deleteSecureString(key: String){
       keychain.deleteSecureString(key)
    }

    // Beware shows prompt!
    func getSecureString(key: String,
                         onSuccess: KeychainSwift.SecureEntryRetrievalSuccessCompletion,
                         onError: KeychainSwift.SecureEntryRetrievalErrorCompletion = { (error) in
    
        print("failed")
    } ) -> Bool {
        return keychain.getSecureString(key: key, onSuccess: onSuccess, onError: onError)
    }
    
    func validateSecureString(key: String, value: String) -> Bool {
        return keychain.validateSecureString(key: key, value: value)
    }
    
    func uidPassword() -> String {

        guard let registrationToken = self.registrationToken else {
            return ""
        }

        guard let uidPasswordCred = (ssoAppName + ":" + registrationToken).toBase64() else {
            return ""
        }

        return "UIDPASSWORD cred=\"" + uidPasswordCred + "\""
    }
    
    func save(appProfile: AppProfileResponse) {
        self.appProfile = appProfile
    }
    
    func save(profile: ProfileResponse) {
        self.profile = profile
    }
    
    func clear() {
        self.registrationToken = nil
        self.userToken = nil
        self.accessToken = nil
        self.storeAccessToken = nil
    }
    
    func requestHeaders(by type: RestAPIClient) -> [String: String] {
        
        var headers = [String: String]()
        
        switch type {
        case .AppProfile:
            break
        case .Register:
            headers["X-IDAAS-SERVICEDOMAIN"] = domain
            break
        case .Authenticate:
            headers["X-IDAAS-SERVICEDOMAIN"]        = domain
            headers["X-IDAAS-REST-AUTHORIZATION"]   = uidPassword()
            break
        case .Validate:
            headers["X-IDAAS-SERVICEDOMAIN"]        = domain
            headers["X-IDAAS-REST-AUTHORIZATION"]   = uidPassword()
            break
        case .Access:
            headers["X-IDAAS-SERVICEDOMAIN"]        = domain
            headers["X-IDAAS-REST-AUTHORIZATION"]       = uidPassword()
            break
        case .AppContext:
            headers["User-Agent"] = "OIC-Agent"
            break
        case .Profile:
            headers["User-Agent"] = "OIC-Agent"
            headers["timestamp-aes"] = "Y7NwjthdwBZmB8wp"
            if let storeAccessToken = storeAccessToken {
                headers["Authorization"] = "OAM-Auth \(storeAccessToken)"
            }
            break
        }
        
        headers["Content-Type"] = "application/json"
        
        return headers
    }
    
}













