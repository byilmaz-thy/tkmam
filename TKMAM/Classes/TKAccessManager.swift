//
//  TKAccessManager.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 18.07.2018.
//  Copyright © 2018 Turkish Airlines. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

@objc public protocol TKAccessManagerDelegate {
    
    func didStartSetup()
    func didFinishSetup()
    func didFailSetup()
    
    func didFinishAuthentication()
    func didFailAuthentication()
    func didRequireAuthentication(completion: (() -> ())?)
    
    func willRequestTouchID()
    func willStartAuthenticationWithTouchID()
    
    @objc optional func whenReachable()
    @objc optional func whenUnReachable()
    
}

public typealias SuccessCompletion = ((Bool) -> ())

public var TKMAM_DEBUG_MODE = false

@objc open class TKAccessManager: NSObject {
    
    @objc public static var shared: TKAccessManager!
    
    private var reachability:       Reachability    = Reachability.init()!
    private var isSetupCompleted:   Bool            = false
    
    private var delegate:           TKAccessManagerDelegate
    
    let accessManagerUrl:   String
    let baseUrl:            String
    let storeUrl:           String
    
    private let serviceDomain:  String
    private let ssoAppName:     String
    private let appId:          String
    
    private let deviceProfile: DeviceProfile = DeviceProfile.generate()
    
    private let loginViewController: UIViewController!
    
    @objc open class func initialize(serviceDomain:         String,
                                     accessManagerUrl:      String,
                                     baseUrl:               String,
                                     appId:                 String,
                                     ssoAppName:            String = "OICSSOApp1",
                                     accessGroup:           String,
                                     delegate:              TKAccessManagerDelegate,
                                     loginViewController:   UIViewController,
                                     storeUrl:              String)
    {
        
        TokenVault.initialize(accessGroup:  accessGroup,
                              binaryId:     appId,
                              ssoAppName:   ssoAppName,
                              domain:       serviceDomain)
        
        TKAccessManager.shared = TKAccessManager(serviceDomain:         serviceDomain,
                                                 accessManagerUrl:      accessManagerUrl,
                                                 baseUrl:               baseUrl,
                                                 appId:                 appId,
                                                 ssoAppName:            ssoAppName,
                                                 accessGroup:           accessGroup,
                                                 delegate:              delegate,
                                                 loginViewController:   loginViewController,
                                                 storeUrl:              storeUrl)
        
        
        NotificationCenter.default.addObserver(shared,
                                               selector:    #selector(TKAccessManager.applicationWillEnterForeground),
                                               name:        UIApplication.willEnterForegroundNotification,
                                               object:      nil)
        
        TKAccessManager.shared.initializeReachability()
        
    }
    
    private init(serviceDomain:         String,
                 accessManagerUrl:      String,
                 baseUrl:               String,
                 appId:                 String,
                 ssoAppName:            String = "OICSSOApp1",
                 accessGroup:           String,
                 delegate:              TKAccessManagerDelegate,
                 loginViewController:   UIViewController,
                 storeUrl:              String)
    {
        
        self.delegate               = delegate
        self.serviceDomain          = serviceDomain
        self.accessManagerUrl       = accessManagerUrl
        self.baseUrl                = baseUrl
        self.ssoAppName             = ssoAppName
        self.appId                  = appId
        self.loginViewController    = loginViewController
        self.storeUrl               = storeUrl
        
        VersionManager.insertVersion()
        
        super.init()
        
    }
    
    // MARK: - Open Functions
    
    @objc open func authenticate(username: String?,
                                 password: String?) {
        
        authenticate(username: username,
                     password: password,
                     with:     nil)
    }
    
    @objc open func setup() {
        
        delegate.didStartSetup()
        
        if isInternetAvailable() {
            
            API.AppProfile.sendRequest(value:               ssoAppName,
                                       body:                deviceProfile.appProfileRequest(serviceDomain: serviceDomain),
                                       responseType:        AppProfileResponse.self,
                                       successsCompletion:  { (appProfile) in
                                        
                                        TokenVault.shared.save(appProfile: appProfile)
                                        
                                        self.isSetupCompleted = true
                                        
                                        self.delegate.didFinishSetup()
                                        
                                        self.autoLogin()
                                        
            }) { (error) in
                
                self.delegate.didFailSetup()
                return true
            }
            
        } else {
            
            guard let _ = TokenVault.shared.appProfile else {
                
                delegate.didFailSetup()
                ACMError.noInternetConnection_1.showAlert()
                
                return
            }
            
            self.isSetupCompleted = true
            
            self.delegate.didFinishSetup()
            
            self.autoLogin()
            
        }
    }
    
    @objc open func requestAccessToken(completion: @escaping SuccessCompletion) {
        requestAccessToken(for: baseUrl, to: false, completion: completion)
    }
    
    @objc open func logout() {
        TokenVault.shared.clear()
        delegate.didRequireAuthentication {
            
        }
    }
    
    @objc open func username() -> String? {
        return UserValidator.shared.username()
    }
    
    @objc open func isInternetAvailable() -> Bool {
        return self.reachability.connection != .none
    }
    
    @objc open func accessHeaders() -> [String: String] {
        var headers: [String : String] = [String : String]()
        headers["User-Agent"] = "OIC-Agent"
        if let accessToken = TokenVault.shared.accessToken {
            headers["Authorization"] = "OAM-Auth \(accessToken)"
     
        }
        return headers
    }
    
    open class func sessionManager(with configuration: URLSessionConfiguration) -> SessionManager {
        
        let sessionManager = SessionManager(configuration: configuration)
        
        let authHandler = TKAuthHandler()
        
        sessionManager.adapter = authHandler
        sessionManager.retrier = authHandler
        
        return sessionManager
    }
    
    // MARK: - Encrypt/Decrypt Functions
    
    @objc open func encryptString(_ str: String) -> String? {
        guard let profile = TokenVault.shared.profile, let userKey = profile.userKey else {
            return nil
        }
        
        return AES256CBC.encryptString(str, password: userKey)
    }
    
    @objc open func decryptString(_ str: String) -> String? {
        guard let profile = TokenVault.shared.profile, let userKey = profile.userKey else {
            return nil
        }
        return AES256CBC.decryptString(str, password: userKey)
    }
    
    @objc open func encryptData(_ data: Data) -> Data? {
        guard let stringToEncrypt = String(data: data, encoding: .utf8) else {
            return nil
        }
        
        return encryptString(stringToEncrypt)?.data(using: .utf8)
    }
    
    @objc open func decryptData(_ data: Data) -> Data? {
        guard let stringToDecrypt = String(data: data, encoding: .utf8) else {
            return nil
        }
        return decryptString(stringToDecrypt)?.data(using: .utf8)
    }
    
    // MARK: - Private Functions
    
    func requestAccessToken(_ retryCount:                Int = 0,
                            for applicationResource:     String,
                            to store:                    Bool = false,
                            completion:                  @escaping SuccessCompletion) {
        
        guard
            let _ = TokenVault.shared.registrationToken,
            let usertoken = TokenVault.shared.userToken else
        {
            
            self.tryTologinWithTouchId(with: { (success) in
                
                if success {
                    
                    self.requestAccessToken(for:        applicationResource,
                                            completion: completion)
                    
                } else {
                    
                    self.logout()
                }
                
            })
            
            return
            
        }
        
        API.AppContext.sendRequest(responseType:        AppContextResponse.self,
                                   to:                  store,
                                   successsCompletion:  { (appContextResponse) in
                                    
                                    let accessRequest = AccessRequest(subjectValue:             usertoken,
                                                                      applicationContext:       appContextResponse.appContext,
                                                                      applicationResource:      applicationResource,
                                                                      subjectType:              "TOKEN",
                                                                      tokenTypeToCreate:        "ACCESSTOKEN")
                                    
                                    API.Access.sendRequest(body: accessRequest,
                                                           responseType: AccessResponse.self,
                                                           successsCompletion: { (accessResponse) in
                                                            
                                                            if store {
                                                                
                                                                TokenVault.shared.storeAccessToken = accessResponse.tokenValue
                                                            } else {
                                                                
                                                                TokenVault.shared.accessToken = accessResponse.tokenValue
                                                            }
                                                            
                                                            completion(true)
                                                            
                                    }, errorCompletion: { (error) -> (Bool) in
                                        
                                        if store {
                                            
                                            completion(false)
                                        } else {
                                            
                                            self.tryTologinWithTouchId(with: { (success) in
                                                
                                                if success {
                                                    
                                                    self.requestAccessToken(for:        applicationResource,
                                                                            completion: completion)
                                                    
                                                } else {
                                                    
                                                    self.logout()
                                                }
                                                
                                            })
                                        }
                                        
                                        return false
                                    })
                                    
        }) { (error) -> (Bool) in
            
            if store {
                
                completion(false)
            } else {
                
                let retryCount = retryCount + 1
                if (retryCount > 5) {
                    self.logout()
                } else {
                    self.requestAccessToken(retryCount,
                                            for: applicationResource,
                                            completion: completion)
                }
            }
            
            
            return false
        }
        
    }
    
    private func autoLogin(with completion: SuccessCompletion? = nil) {
        
        guard let _ = TokenVault.shared.registrationToken, let userToken = TokenVault.shared.userToken else {
            self.tryTologinWithTouchId(with: completion)
            return
        }
        
        let validateRequest = ValidateRequest(subjectValue: userToken, subjectType: "TOKEN")
        
        API.Validate.sendRequest(body: validateRequest,
                                 responseType: ValidateResponse.self,
                                 successsCompletion: { (validateResponse) in
                                    
                                    if let completion = completion {
                                        
                                        completion(true)
                                        
                                    } else {
                                        
                                        self.getAppProfile()
                                        
                                    }
                                    
        }) { (error) -> (Bool) in
            
            self.tryTologinWithTouchId(with: completion)
            return false
        }
        
    }
    
    private func authenticate(username: String?, password: String?, with completion: SuccessCompletion?) {
        
        guard let _ = TokenVault.shared.appProfile else {
            
            if let completion = completion {
                
                completion(false)
                
            } else {
                
                delegate.didFailAuthentication()
                ACMError.somethingWentWrong.showAlert()
                
            }
            
            return
        }
        
        guard let username = username, let password = password else {
            
            if let completion = completion {
                
                completion(false)
                
            } else {
                
                delegate.didFailAuthentication()
                ACMError.usernameOrPassword.showAlert()
                
            }
            
            return
        }
        
        guard username != "" && password != "" else {
            
            if let completion = completion {
                
                completion(false)
                
            } else {
                
                delegate.didFailAuthentication()
                ACMError.usernameOrPassword.showAlert()
                
            }
            
            return
        }
        
        if isInternetAvailable() {
            
            let registerRequest = RegisterRequest(subjectType:          "USERCREDENTIAL",
                                                  username:             username,
                                                  password:             password,
                                                  tokenTypeToCreate:    "CLIENTREGHANDLE",
                                                  deviceProfile:        deviceProfile,
                                                  clientId:             ssoAppName)
            
            API.Register.sendRequest(body:                  registerRequest,
                                     responseType:          RegisterResponse.self,
                                     successsCompletion:    { (registerResponse) in
                                        
                                        TokenVault.shared.registrationToken = registerResponse.tokenValue
                                        
                                        let authRequest = AuthenticateRequest(subjectType:          "USERCREDENTIAL",
                                                                              username:             username,
                                                                              password:             password,
                                                                              tokenTypeToCreate:    "USERTOKEN",
                                                                              deviceProfile:        self.deviceProfile)
                                        
                                        
                                        API.Authenticate.sendRequest(body:                  authRequest,
                                                                     responseType:          AuthenticateResponse.self,
                                                                     successsCompletion:    { (authenticateResponse) in
                                                                        
                                                                        TokenVault.shared.userToken = authenticateResponse.tokenValue
                                                                        
                                                                        if let completion = completion {
                                                                            
                                                                            completion(true)
         
                                                                        } else {
                                                                            
                                                                            self.delegate.willRequestTouchID()
                                                                            UserValidator.shared.activate(username: username, password: password) {
                                                                                
                                                                                self.delegate.willStartAuthenticationWithTouchID()
                                                                                self.getAppProfile()
                                                                                
                                                                            }
                                                                        }
                                                                        
                                                                        
                                        }, errorCompletion: { (error) -> (Bool) in
                                            
                                            if let completion = completion {
                                                
                                                completion(false)
                                                return false
                                                
                                            } else {
                                                
                                                self.delegate.didFailAuthentication()
                                                return true
                                                
                                            }
                                        })
                                        
                                        
            }) { (error) -> (Bool) in
                
                if let completion = completion {
                    
                    completion(false)
                    return false
                    
                } else {
                    
                    self.delegate.didFailAuthentication()
                    return true
                    
                }
            }
            
            
            
            
        } else {
            
            if UserValidator.shared.validateBy(username: username, password: password) {
                
                if let completion = completion {
                    
                    completion(true)
                } else {
                    
                    self.getAppProfile()
                }
                
            } else {
                
                if let completion = completion {
                    
                    completion(false)
                    
                } else {
                    
                    delegate.didFailAuthentication()
                    ACMError.wrongUsernameOrPassword.showAlert()
                    
                }
            }
            
        }
        
    }
    
    private func tryTologinWithTouchId(with completion: SuccessCompletion? = nil) {
        
        delegate.willRequestTouchID()
        
        UserValidator.shared.validateUser(completion: { (isValidateble, username, password) in
            
            if isValidateble {
                if let username = username, let password = password {
                    
                    self.delegate.willStartAuthenticationWithTouchID()
                    
                    self.authenticate(username: username,
                                      password: password,
                                      with: completion)
                    
                } else {
                    if let completion = completion { completion(false) }
                }
            } else {
                if let completion = completion { completion(false) }
            }
        })
    }
    
    private func getAppProfile() {
        
        self.requestAccessToken(for: storeUrl, to: true) { (success) in
            
            if success {
                
                let profileRequest = ProfileRequest(appId:              self.appId,
                                                    deviceUUID:         self.deviceProfile.udid,
                                                    brand:              "Apple",
                                                    model:              UIDevice.modelName,
                                                    platform:           "IOS",
                                                    osVersion:          self.deviceProfile.osversion,
                                                    appBinaryVersion:   Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)
                
                
                API.Profile.sendRequest(body: profileRequest,
                                        responseType: ProfileResponse.self,
                                        successsCompletion: { (profileResponse) in
                                            
                                            TokenVault.shared.profile = profileResponse
                                            
                                            self.handleProfileResponse(profileResponse)
                                            
                },
                                        errorCompletion: { (error) -> (Bool) in
                                            
                                            if let profileResponse = TokenVault.shared.profile {
                                                
                                                self.handleProfileResponse(profileResponse)
                                                return false
                                            } else {
                                                
                                                self.delegate.didFailAuthentication()
                                                
                                                return false
                                            }
                                            
                })
                
            } else {
                
                if let profileResponse = TokenVault.shared.profile {
                    
                    self.handleProfileResponse(profileResponse)
                } else {
                    
                    self.delegate.didFailAuthentication()
                    ACMError.somethingWentWrong_2.showAlert()
                    
                }
            }
            
        }
        
        
    }
    
    private func handleProfileResponse(_ response: ProfileResponse) {
        
//        yetki -> TK Store -> root -> update
        
        guard response.userAuthorizedToSeeProfile else {
            ACMError.notAuthorized.showAlert()
            self.delegate.didFailAuthentication()
            return
        }
        
        //TODO: check tkstore is installed.
        
        if response.jailBreakProtection {
            if DeviceProfile.hasDeviceJailbreak() {
                ACMError.jailBreak.showAlert()
                self.delegate.didFailAuthentication()
                return
            }
        }
        
        let appDisplayName = Bundle.main.infoDictionary!["CFBundleName"] as! String
        let title = "Warning"
        let message = "A New version of " + appDisplayName +  " is available. Please install it from TK Store."
        
        var actions = [UIAlertAction]()
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            response.openDownloadUrl()
            exit(0)
        })
        
        actions.append(okAction)
        
        let cancelAction = UIAlertAction(title: "Later", style: .default, handler: { (action) in
            self.delegate.didFinishAuthentication()
        })
        
        switch response.updateType {
        case .none:
            self.delegate.didFinishAuthentication()
            break
        case .optional:
            actions.append(cancelAction)
            fallthrough
        case .force:
            Alert.show(title: title, message: message, actions: actions)
            break
        }
        
    }
    
    @objc func applicationWillEnterForeground() {
        if Alert.topViewController()!.isKind(of: self.loginViewController.classForCoder)
            && !isSetupCompleted {
            setup()
        }
    }
    
    private func initializeReachability() {
        do {
            try reachability.startNotifier()
            reachability.whenReachable = { reachability in
                self.whenReachable()
            }
            reachability.whenUnreachable = { reachability in
                self.whenUnReachable()
            }
        } catch {
            Swift.debugPrint("TKMAM : Unable to proceed with Reachability")
        }
    }
    
    private func whenReachable() {
        
        delegate.whenReachable?()
        
        if !isSetupCompleted {
            
            setup()
        }
        
    }
    
    private func whenUnReachable() {
        
        delegate.whenUnReachable?()
    }
}
