//
//  TKUserValidator.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 05/03/2018.
//

import Foundation
import LocalAuthentication

public enum TKUserValidatorStatus: String {
    case touchId = "touchId"
    case password = "password"
    case none = "none"
}

typealias UserValidator = TKUserValidator

open class TKUserValidator: NSObject {
    
    private let TKMAM_USERVALIDATOR_STATUS = "TKMAM_USERVALIDATOR_STATUS"
    private let TKMAM_USERVALIDATOR_USERNAME = "TKMAM_USERVALIDATOR_USERNAME"
    private let TKMAM_USERVALIDATOR_PASSWORD = "TKMAM_USERVALIDATOR_PASSWORD"
    
    public static let shared: TKUserValidator = TKUserValidator()
    
    private var authenticationContext = LAContext()
    
    
    override private init() {
        super.init()
        authenticationContext.localizedFallbackTitle = ""
    }
    
    func activate(username: String, password: String, completion: @escaping () -> ()) {
        
        let chainedUsername = TokenVault.shared.get(key: TKMAM_USERVALIDATOR_USERNAME) ?? ""
        
        if chainedUsername != username {
            
            TokenVault.shared.delete(key: TKMAM_USERVALIDATOR_USERNAME)
            TokenVault.shared.deleteSecureString(key: TKMAM_USERVALIDATOR_PASSWORD)
            
            setStatus(status: .none)
        }
        
        let status = getStatus()
        
        switch status {
        case .touchId, .password:
            if (chainedUsername == username) {
                let c = TokenVault.shared.setSecureString(key: self.TKMAM_USERVALIDATOR_PASSWORD, value: password) // TODO: delete c
                
            }
            completion()
            break
        case .none:
            if authenticationContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil) {
                
                alert(title: getBiometricAuthName(),
                      message: "Do you want to enable " + getBiometricAuthName() + " authentication?",
                      okTitle: "Yes",
                      cancelTitle: "No",
                      okHandler: { (action) in
                        
                        self.touchIdAuth(completion: { (success) in
                            if success {
                                TokenVault.shared.set(key: self.TKMAM_USERVALIDATOR_USERNAME, value: username)
                                TokenVault.shared.setSecureString(key: self.TKMAM_USERVALIDATOR_PASSWORD, value: password)
                                
                                self.setStatus(status: .touchId)
                                completion()
                            } else {
                                
                                self.alert(title: "ERROR", message: "TouchID password match failed!",
                                           okTitle: "OK",
                                           okHandler: { (_) in
                                           
                                            self.setStatus(status: .password)
                                            completion()
                                },
                                           cancelHandler: nil,
                                           show: true)
                            }
                        })
                        
                },
                      cancelHandler: { (action) in
                        self.setStatus(status: .password)
                        completion()
                })
                
            } else {
                completion()
            }
          
        }
    }
    
    func validateUser(completion: @escaping (Bool,String?,String?) -> ()){
        
        let status = getStatus()
        
        switch status {
        case .touchId:
            
            TokenVault.shared.getSecureString(key: TKMAM_USERVALIDATOR_PASSWORD,
                                              onSuccess: { (password) in
                
                                                guard let username = TokenVault.shared.get(key: self.TKMAM_USERVALIDATOR_USERNAME) else {
                                                    completion(true,nil, nil)
                                                    return
                                                }
                                                
                                                completion(true,username, password)
            }) { (error) in
                if #available(iOS 11.3, *) {
                    let c  = SecCopyErrorMessageString(error, nil) as String? ?? "Unknown error"
                    print(c)
                }
            
                completion(true,nil,nil)
            }
        case .password:
            completion(true,nil,nil)
            break
        case .none:
            completion(false,nil,nil)
            break;
        }
    }
    
    func validateBy(username: String, password: String) -> Bool{
        let tokenVault = TokenVault.shared!
        
        guard username == tokenVault.get(key: self.TKMAM_USERVALIDATOR_USERNAME),
            tokenVault.validateSecureString(key: TKMAM_USERVALIDATOR_PASSWORD, value: password) else {
                return false
        }
        
        return true
    }
    
    func username() -> String? {
        return TokenVault.shared.get(key: TKMAM_USERVALIDATOR_USERNAME)
    }
    
    @discardableResult
    private func alert(title: String,
                       message: String,
                       okTitle: String,
                       cancelTitle: String = "",
                       okHandler: ((UIAlertAction) -> Void)?,
                       cancelHandler: ((UIAlertAction) -> Void)? = nil,
                       show: Bool = true) -> UIAlertController {
        
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: okTitle, style: .default, handler: okHandler)
        let noAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler)
        alertController.addAction(yesAction)
        if let _ = cancelHandler {
            alertController.addAction(noAction)
        }
        if show { Alert.topViewController()!.present(alertController, animated: true, completion: nil) }
        return alertController
    }
    
    private func touchIdAuth(completion: @escaping (Bool) -> ()) {
        self.authenticationContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: getBiometricAuthName(), reply: { (succes, error) in
            if succes {
                completion(true)
            } else {
                switch error! {
                case LAError.appCancel:
                    self.touchIdAuth(completion: completion)
                    break
                case LAError.authenticationFailed:
                    self.touchIdAuth(completion: completion)
                    break
                case LAError.invalidContext:
                    self.touchIdAuth(completion: completion)
                    break
                case LAError.passcodeNotSet:
                    completion(false)
                    break
                case LAError.systemCancel:
                    self.touchIdAuth(completion: completion)
                    break
                case LAError.touchIDLockout:
                    completion(false)
                    break
                case LAError.touchIDNotAvailable:
                    completion(false)
                    break
                case LAError.userCancel:
                    completion(false)
                    break
                case LAError.userFallback:
                    completion(false)
                    break
                default:
                    completion(false)
                    break
                }
            }
        })
    }
    
    
    private func getStatus() -> TKUserValidatorStatus {
        if let stringStatus = TokenVault.shared.get(key: TKMAM_USERVALIDATOR_STATUS) {
            return TKUserValidatorStatus(rawValue: stringStatus)!
        } else {
            return TKUserValidatorStatus.none
        }
    }
    
    private func setStatus(status: TKUserValidatorStatus){
        TokenVault.shared.set(key: TKMAM_USERVALIDATOR_STATUS, value: status.rawValue)
    }
    
    private func getBiometricAuthName() -> String{
        return (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436) ? "Face ID" : "Touch ID" //TODO: its wrong, FIX IT
    }
    
    open func set(enabled: Bool){
        if enabled {
            
            TokenVault.shared.delete(key: TKMAM_USERVALIDATOR_USERNAME)
            TokenVault.shared.deleteSecureString(key: TKMAM_USERVALIDATOR_PASSWORD)

            setStatus(status: .none)
        } else {
            setStatus(status: .password)
        }
    }
    
    open func isEnabled() -> Bool{
        return getStatus() != .password
    }

}
