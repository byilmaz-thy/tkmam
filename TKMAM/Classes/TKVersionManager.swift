//
//  TKVersionManager.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 18.07.2018.
//  Copyright © 2018 Turkish Airlines. All rights reserved.
//

import Foundation

public enum AppStatus {
    case installed
    case updateRequired
    case notInstalled
}

typealias VersionManager = TKVersionManager

open class TKVersionManager {
    
    class func insertVersion(){
        if let bundleID = Bundle.main.bundleIdentifier {
            if let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                TokenVault.shared.set(key: bundleID, value: buildNumber)
            }
        }
    }
    
    open class func getVersion() -> String {
        if let bundleID = Bundle.main.bundleIdentifier {
            return  TokenVault.shared.get(key: bundleID) ?? ""
        }
        return ""
    }
    
    class func checkVersion(buildNumber: String) -> AppStatus {
        
        guard let currentBuildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else {
            return .installed
        }
        
        guard let buildNumberInt = Int(buildNumber), let currentBuildNumberInt = Int(currentBuildNumber) else {
            return .updateRequired
        }
        
        if currentBuildNumberInt >= buildNumberInt {
            return .installed
        } else {
            return .updateRequired
        }
        
    }

    open class func appStatus(bundleId: String? = nil, appUrlScheme: String, buildNumber: String) -> AppStatus {
        
        var currentBundleId: String
        
        if let bundleId = bundleId {
            currentBundleId = bundleId
        } else {
            currentBundleId = Bundle.main.bundleIdentifier!
        }
        
        guard let urlScheme = URL(string: appUrlScheme) else {
            return .notInstalled
        }
        
        if !UIApplication.shared.canOpenURL(urlScheme) {
            return .notInstalled
        }
        
        guard let keychainVersion = TokenVault.shared.get(key: currentBundleId) else {
            return .installed
        }
        
        guard let keychainVersionInt = Int(keychainVersion), let buildNumberInt = Int(buildNumber) else {
            return .updateRequired
        }
        
        if buildNumberInt >= keychainVersionInt {
            return .installed
        } else {
            return .updateRequired
        }
    }
}
