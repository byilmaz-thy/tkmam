//
//  TKRequestAdapter.swift
//  TKMAM
//
//  Created by YAVUZ ÖZTÜRK on 7.08.2018.
//

import Alamofire

typealias AuthHandler = TKAuthHandler

open class TKAuthHandler: RequestAdapter, RequestRetrier {
    
    let Access_Control_Expose_Headers   = "Access-Control-Expose-Headers"
    let WWW_Authenticate                = "WWW-Authenticate"
    
    private let lock = NSLock()
    
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    public init() {}
    
    // MARK: - RequestAdapter
    
    open func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        
        var urlRequest = urlRequest
        
        for (key, value) in TKAccessManager.shared.accessHeaders() {
            urlRequest.setValue(value, forHTTPHeaderField: key)
        }
        
        return urlRequest
    }
    
    // MARK: - RequestRetrier
    
    open func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        
        lock.lock() ; defer { lock.unlock() }
        
        guard let httpUrlResponse = request.task?.response as? HTTPURLResponse else {
            completion(false, 0.0)
            return
        }
        
        guard let exposeHeader = httpUrlResponse.allHeaderFields[Access_Control_Expose_Headers] as? String, exposeHeader == WWW_Authenticate else {
            completion(false, 0.0)
            return
        }
        
        if httpUrlResponse.statusCode == 401 {
            
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                
                refresh { [weak self] (success) in
                    
                    guard let strongSelf = self else { return }
                    
                    strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
                    
                    strongSelf.requestsToRetry.forEach({ (requestRetryCompletion) in requestRetryCompletion(success, 0.0) })
                    strongSelf.requestsToRetry.removeAll()
                    
                }
            }
            
        } else {
            completion(false, 0.0)
        }
        
    }
    
    func refresh(completion: @escaping SuccessCompletion) {
        guard !isRefreshing else { return }
        
        isRefreshing = true
        
        TKAccessManager.shared.requestAccessToken(for: TKAccessManager.shared.baseUrl) { [weak self] (success) in
            
            guard let strongSelf = self else { return }
            
            completion(success)
            
            strongSelf.isRefreshing = false
        }
    }
    
}
